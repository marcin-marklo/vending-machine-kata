package tdd.vendingMachine;

public class Product {

    public String name;
    public int cost;

    public Product(String name, int cost) {
        this.name = name;
        this.cost = cost;
    }

    public Product(Product product) {
        name = product.name;
        cost = product.cost;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Product)) {
            return false;
        }
        Product other = (Product)obj;
        return (other.cost == cost) && (other.name.equals(name));
    }

}
