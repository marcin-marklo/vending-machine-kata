package tdd.vendingMachine.demo;

import java.util.Optional;
import java.util.Scanner;

import tdd.vendingMachine.Coin;
import tdd.vendingMachine.VendingMachine;
import tdd.vendingMachine.VendingMoneyKeeper;
import tdd.vendingMachine.VendingProductManager;
import tdd.vendingMachine.fsm.VendingStateFactory;

public class ConsoleDemo {

    public static void main(String[] args) {
        System.out.println("Welcome to the Vending Machine demo!");
        System.out.println("To insert coins, type: $<amount> (eg. $0.1)");
        System.out.println("To quit, type POWEROFF at any state.");
        System.out.println("To add products, go to the SERVICE state.");
        System.out.println("Happy vending!");
        System.out.println("-------------------------------------------");
        final VendingMachine.Internals internals = new VendingMachine.Internals();
        internals.coinReturnSlot = c -> System.out.println("Returned coin: " + Coin.formatAsDecimal(c.denomination));
        internals.display = str -> System.out.print(str);
        internals.manager = new VendingProductManager();
        internals.moneyKeeper = new VendingMoneyKeeper();
        internals.productTray = p -> System.out.println("Product: " + p.name);
        final VendingMachine machine = new VendingMachine(internals, 8, new VendingStateFactory());
        final Scanner sc = new Scanner(System.in);
        while (true) {
            if (sc.hasNextInt()) {
                final int number = sc.nextInt();
                sc.nextLine();
                machine.onKeyboard(number);
                continue;
            }
            final String command = sc.nextLine().trim();
            if (command.startsWith("POWEROFF")) {
                break;
            }
            if (command.startsWith("$")) {
                final float floatCoin = Float.parseFloat(command.substring(1));
                final Optional<Coin> maybeCoin = Coin.getCoin(floatCoin);
                if (maybeCoin.isPresent()) {
                    machine.onCoinInsert(maybeCoin.get());
                } else {
                    System.out.println("Invalid coin!");
                }
                continue;
            }
            machine.onCommand(command);
        }
        sc.close();
    }

}
