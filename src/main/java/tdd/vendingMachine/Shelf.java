package tdd.vendingMachine;

import java.util.Optional;

public class Shelf {

    public Optional<Product> product;
    public int count;

    public Shelf() {
        product = Optional.empty();
        count = 0;
    }

    public Shelf(Product product, int count) {
        this.product = Optional.ofNullable(product);
        this.count = count;
    }

}
