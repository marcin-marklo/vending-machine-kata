package tdd.vendingMachine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.function.Consumer;

public class VendingMoneyKeeper implements MoneyKeeper {

    EnumMap<Coin, Integer> coins = new EnumMap<>(Coin.class);

    @Override
    public void addMoney(Coin coin) {
        coins.compute(coin, (k, v) -> (v == null) ? 1 : v + 1);
    }

    @Override
    public boolean canGetMoney(int amount) {
        final float balance = iterateForAmount(amount, c -> {});
        return balance == 0;
    }

    @Override
    public Collection<Coin> getMoney(int amount) {
        final List<Coin> toGive = new ArrayList<>();
        final float balance = iterateForAmount(amount, c -> toGive.add(c));
        if (balance > 0) {
            throw new NotEnoughMoneyException("Not enough money to return!");
        }
        return toGive;
    }

    private float iterateForAmount(int amount, Consumer<Coin> consumer) {
        for (Coin coin : Coin.values()) {
            if (amount == 0) {
                return 0;
            }
            int count = coins.getOrDefault(coin, 0);
            while ((count > 0) && (amount >= coin.denomination)) {
                amount -= coin.denomination;
                --count;
                consumer.accept(coin);
            }
        }
        return amount;
    }

    @Override
    public boolean removeCoin(Coin coin) {
        final int amount = coins.getOrDefault(coin, 0);
        if (amount == 0) {
            return false;
        }
        coins.put(coin, amount - 1);
        return true;
    }

    static class NotEnoughMoneyException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        private NotEnoughMoneyException(String msg) {
            super(msg);
        }
    }

}
