package tdd.vendingMachine;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class VendingProductManager implements ProductManager {

    private final Map<String, Product> products = new HashMap<>();

    @Override
    public void addProduct(String name, int cost) throws InvalidOperationException {
        if (products.containsKey(name)) {
            throw new InvalidOperationException("Product already exists: " + name);
        }
        if (!isCostValid(cost)) {
            throw new InvalidOperationException("Invalid cost for product: " + cost);
        }
        products.put(name, new Product(name, cost));
    }

    private boolean isCostValid(int cost) {
        if (cost < 0) {
            return false;
        }
        final Coin[] coins = Coin.values();
        final int lowest = coins[coins.length - 1].denomination;
        return (cost % lowest) == 0;
    }

    @Override
    public void modifyProduct(String productName, String newName) throws InvalidOperationException {
        if ((newName == null) || (newName.isEmpty())) {
            throw new InvalidOperationException("Name cannot be empty!");
        }
        final Product product = products.remove(productName);
        if (product == null) {
            throw new InvalidOperationException("Product does not exist: " + productName);
        }
        product.name = newName;
        products.put(newName, product);
    }

    @Override
    public void modifyProduct(String productName, int newCost) throws InvalidOperationException {
        if (!isCostValid(newCost)) {
            throw new InvalidOperationException("Invalid product cost: " + newCost);
        }
        final Product product = products.get(productName);
        if (product == null) {
            throw new InvalidOperationException("Product does not exist: " + productName);
        }
        product.cost = newCost;
    }

    @Override
    public void modifyProduct(String productName, String newName, int newCost) throws InvalidOperationException {
        modifyProduct(productName, newCost);
        modifyProduct(productName, newName);
    }

    @Override
    public Optional<Product> getProductByName(String name) {
        final Product product = products.get(name);
        return Optional.ofNullable(product);
    }

    @Override
    public void removeProduct(Product product) {
        products.remove(product.name);
    }

    @Override
    public String listProducts() {
        final StringBuilder sb = new StringBuilder();
        products.forEach((s, p) -> sb
                .append(s)
                .append(": ")
                .append(Coin.formatAsDecimal(p.cost))
                .append("\n")
                );
        return sb.toString();
    }

    Collection<Product> getProducts() {
        return products.values();
    }

}
