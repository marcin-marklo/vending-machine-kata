package tdd.vendingMachine;

import java.util.function.Consumer;

import tdd.vendingMachine.fsm.StateContext;
import tdd.vendingMachine.fsm.StateFactory;
import tdd.vendingMachine.fsm.VendingStateContext;

public class VendingMachine {

    private final Consumer<String> display;
    private final Consumer<Coin> coinReturnSlot;
    private final Shelf[] shelves;
    private final MoneyKeeper moneyKeeper;
    private final Consumer<Product> productTray;
    private final ProductManager manager;
    private final StateContext context;
    private final StateFactory factory;

    public VendingMachine(Internals internals, int shelfCount, StateFactory factory) {
        display = internals.display;
        coinReturnSlot = internals.coinReturnSlot;
        moneyKeeper = internals.moneyKeeper;
        productTray = internals.productTray;
        manager = internals.manager;
        shelves = new Shelf[shelfCount];
        for (int i = 0; i < shelfCount; i++) {
            shelves[i] = new Shelf(null, 0);
        }
        context = new VendingStateContext(
                display,
                coinReturnSlot,
                shelves,
                moneyKeeper,
                productTray,
                manager
                );
        this.factory = factory;
        context.changeState(factory.getIdleState(context));
    }

    public void onCoinInsert(Coin coin) {
        context.onCoinInsert(coin);
    }

    public void onKeyboard(int number) {
        context.onKeyboard(number);
    }

    public void onCommand(String cmd) {
        context.onCommand(cmd);
    }

    public static class Internals {
        public Consumer<String> display;
        public Consumer<Coin> coinReturnSlot;
        public MoneyKeeper moneyKeeper;
        public Consumer<Product> productTray;
        public ProductManager manager;
    }

}
