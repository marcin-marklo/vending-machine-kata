package tdd.vendingMachine;

import java.util.Optional;

public enum Coin {

    FIVE(500),
    TWO(200),
    ONE(100),
    HALF(50),
    ONE_FIFTH(20),
    ONE_TENTH(10);

    public final int denomination;

    private Coin(int denomination) {
        this.denomination = denomination;
    }

    public int getDenomination() {
        return denomination;
    }

    public static Optional<Coin> getCoin(float number) {
        int denomination = (int)(number * 100);
        Coin coin = null;
        for (Coin c : Coin.values()) {
            if (c.denomination == denomination) {
                coin = c;
                break;
            }
        }
        return Optional.ofNullable(coin);
    }

    public static String formatAsDecimal(int amount) {
        return String.format("%d.%02d", amount / 100, amount % 100);
    }

}
