package tdd.vendingMachine;

import java.util.Collection;

public interface MoneyKeeper {

    void addMoney(Coin coin);

    boolean canGetMoney(int amount);

    Collection<Coin> getMoney(int amount);

    boolean removeCoin(Coin coin);

}