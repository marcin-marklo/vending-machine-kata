package tdd.vendingMachine.fsm;

import tdd.vendingMachine.Coin;
import tdd.vendingMachine.MoneyKeeper;
import tdd.vendingMachine.Product;
import tdd.vendingMachine.ProductManager;
import tdd.vendingMachine.Shelf;

public interface StateContext {

    public void changeState(State newState);
    public void display(String message);
    public void returnCoin(Coin coin);
    public void deliverProduct(Product product);
    public Shelf[] getShelves();
    public MoneyKeeper getMoneyKeeper();
    public ProductManager getProductManager();

    public void onCoinInsert(Coin coin);
    public void onKeyboard(int number);
    public void onCommand(String cmd);

}
