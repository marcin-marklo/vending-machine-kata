package tdd.vendingMachine.fsm;

import java.util.ArrayList;
import java.util.List;

import tdd.vendingMachine.Coin;
import tdd.vendingMachine.Shelf;

class InsertCoinsState extends AbstractState {

    private final Shelf shelf;
    private int remaining;
    private List<Coin> insertedCoins = new ArrayList<>();

    InsertCoinsState(StateFactory factory, StateContext context, Shelf shelf) {
        super(factory, context);
        this.shelf = shelf;
        remaining = shelf.product.map(p -> p.cost).orElse(0);
    }

    @Override
    public void onEnter() {
        context.display(String.format("Left to pay: %s", Coin.formatAsDecimal(remaining)));
    }

    @Override
    public void onCoinInsert(Coin coin) {
        insertedCoins.add(coin);
        remaining -= coin.denomination;
        context.getMoneyKeeper().addMoney(coin);
        if (remaining > 0) {
            context.display(String.format("Left to pay: %.2f", remaining * 0.01f));
        } else {
            final int change = -remaining;
            if (context.getMoneyKeeper().canGetMoney(change)) {
                context.changeState(factory.getDeliverProductState(context, shelf, change));
            } else {
                context.display("Can not deliver change\n");
                context.display("Please collect your money.\n");
                cleanup();
            }
        }
    }

    @Override
    public void onCommand(String cmd) {
        switch (cmd) {
        case State.COMMAND_CANCEL:
            cleanup();
            break;
        default:
            super.onCommand(cmd);
        }
    }

    private void cleanup() {
        insertedCoins.forEach(
                c -> {
                    context.returnCoin(c);
                    context.getMoneyKeeper().removeCoin(c);
                });
        context.changeState(factory.getIdleState(context));
    }

}
