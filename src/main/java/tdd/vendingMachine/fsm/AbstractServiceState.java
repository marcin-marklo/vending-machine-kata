package tdd.vendingMachine.fsm;

import tdd.vendingMachine.Coin;

abstract class AbstractServiceState extends AbstractState {

    AbstractServiceState(StateFactory factory, StateContext context) {
        super(factory, context);
    }

    @Override
    public void onCoinInsert(Coin coin) {
        context.getMoneyKeeper().addMoney(coin);
    }


}
