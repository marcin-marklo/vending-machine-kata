package tdd.vendingMachine.fsm;

import tdd.vendingMachine.Shelf;

public class VendingStateFactory implements StateFactory {

    @Override
    public State getIdleState(StateContext context) {
        return new IdleState(this, context);
    }

    @Override
    public State getInsertCoinsState(StateContext context, Shelf shelf) {
        return new InsertCoinsState(this, context, shelf);
    }

    @Override
    public State getDeliverProductState(StateContext context, Shelf shelf, int change) {
        return new DeliverProductState(this, context, shelf, change);
    }

    @Override
    public State getDeliverChangeState(StateContext context, int change) {
        return new DeliverChangeState(this, context, change);
    }

    @Override
    public State getMainServiceState(StateContext context) {
        return new MainServiceState(this, context);
    }

    @Override
    public State getModifyShelfState(StateContext context, Shelf shelf) {
        return new ModifyShelfState(this, context, shelf);
    }

    @Override
    public State getModifyProductsState(StateContext context) {
        return new ModifyProductsState(this, context);
    }

}
