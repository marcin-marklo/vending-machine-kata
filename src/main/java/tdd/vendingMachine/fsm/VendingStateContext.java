package tdd.vendingMachine.fsm;

import java.util.function.Consumer;

import tdd.vendingMachine.Coin;
import tdd.vendingMachine.MoneyKeeper;
import tdd.vendingMachine.Product;
import tdd.vendingMachine.ProductManager;
import tdd.vendingMachine.Shelf;

public class VendingStateContext implements StateContext {

    private State state;

    private final Consumer<String> display;
    private final Consumer<Coin> coinReturnSlot;
    private final Shelf[] shelves;
    private final MoneyKeeper moneyKeeper;
    private final Consumer<Product> productTray;
    private final ProductManager productManager;

    public VendingStateContext(
            Consumer<String> display,
            Consumer<Coin> coinReturnSlot,
            Shelf[] shelves,
            MoneyKeeper moneyKeeper,
            Consumer<Product> productTray,
            ProductManager productManager
            ) {
        this.display = display;
        this.coinReturnSlot = coinReturnSlot;
        this.shelves = shelves;
        this.moneyKeeper = moneyKeeper;
        this.productTray = productTray;
        this.productManager = productManager;
    }

    @Override
    public void changeState(State newState) {
        state = newState;
        state.onEnter();
    }

    @Override
    public void display(String message) {
        display.accept(message);
    }

    @Override
    public void returnCoin(Coin coin) {
        coinReturnSlot.accept(coin);;
    }

    @Override
    public Shelf[] getShelves() {
        return shelves;
    }

    @Override
    public MoneyKeeper getMoneyKeeper() {
        return moneyKeeper;
    }

    @Override
    public void deliverProduct(Product product) {
        productTray.accept(product);;
    }

    @Override
    public ProductManager getProductManager() {
        return productManager;
    }

    @Override
    public void onCoinInsert(Coin coin) {
        state.onCoinInsert(coin);
    }

    @Override
    public void onKeyboard(int number) {
        state.onKeyboard(number);
    }

    @Override
    public void onCommand(String cmd) {
        state.onCommand(cmd);
    }

}
