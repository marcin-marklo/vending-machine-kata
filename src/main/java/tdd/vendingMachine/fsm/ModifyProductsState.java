package tdd.vendingMachine.fsm;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tdd.vendingMachine.Product;
import tdd.vendingMachine.ProductManager;
import tdd.vendingMachine.ProductManager.InvalidOperationException;

class ModifyProductsState extends AbstractServiceState {

    static final String MODIFY_COMMAND = "MODIFY";
    static final String ADD_COMMAND = "ADD";
    static final String DELETE_COMMAND = "DELETE";
    private static final Pattern modifyPattern = Pattern.compile(MODIFY_COMMAND + " (.+?) -> ([A-Za-z].*?)? ?([\\d.]+)?");
    private static final Pattern addPattern = Pattern.compile(ADD_COMMAND + " ([\\w ]+) ([\\d.]+)");


    ModifyProductsState(StateFactory factory, StateContext context) {
        super(factory, context);
    }

    @Override
    public void onEnter() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Available products:\n");
        sb.append(context.getProductManager().listProducts()).append("\n");
        sb.append("Available commands: \n");
        sb.append(COMMAND_CANCEL).append(" - return to the main service state\n");
        sb.append(MODIFY_COMMAND).append(" <product name> -> <new name> <new cost> - modify the given product\n");
        sb.append(ADD_COMMAND).append(" <product name> <cost> - add a new product\n");
        sb.append(DELETE_COMMAND).append(" <product name> - remove the given product\n");
        context.display(sb.toString());
    }

    @Override
    public void onCommand(String cmd) {
        if (cmd.equals(State.COMMAND_CANCEL)) {
            context.changeState(factory.getMainServiceState(context));
            return;
        }
        try {
            if (cmd.startsWith(MODIFY_COMMAND) && executeModifyCommand(cmd)) {
                return;
            }
            if (cmd.startsWith(ADD_COMMAND) && executeAddCommand(cmd)) {
                return;
            }
            if (cmd.startsWith(DELETE_COMMAND) && executeRemoveCommand(cmd)) {
                return;
            }
            super.onCommand(cmd);
        } catch (InvalidOperationException ex) {
            context.display(ex.getMessage());
        }
    }

    private boolean executeModifyCommand(String cmd) throws InvalidOperationException {
        final Matcher matcher = modifyPattern.matcher(cmd);
        if (!matcher.matches()) {
            return false;
        }
        final String name = matcher.group(1);
        final String newName = matcher.group(2);
        final String costStr = matcher.group(3);
        if (costStr != null) {
            final int cost = (int)(Float.parseFloat(costStr) * 100);
            if (newName != null) {
                context.getProductManager().modifyProduct(name, newName, cost);
            } else {
                context.getProductManager().modifyProduct(name, cost);
            }
        }
        if (newName != null) {
            context.getProductManager().modifyProduct(name, newName);
        }
        return true;
    }

    private boolean executeAddCommand(String cmd) throws InvalidOperationException {
        final Matcher matcher = addPattern.matcher(cmd);
        if (!matcher.matches()) {
            return false;
        }
        final String name = matcher.group(1);
        final int cost = (int)(Float.parseFloat(matcher.group(2)) * 100);
        context.getProductManager().addProduct(name, cost);
        return true;
    }

    private boolean executeRemoveCommand(String cmd) {
        final ProductManager manager = context.getProductManager();
        final String[] splitCommand = cmd.split(" +");
        if (splitCommand.length < 2) {
            return false;
        }
        final String name = splitCommand[1];
        final Optional<Product> maybeProduct = manager.getProductByName(name);
        if (!maybeProduct.isPresent()) {
            return false;
        }
        manager.removeProduct(maybeProduct.get());
        return true;
    }

}
