package tdd.vendingMachine.fsm;

import tdd.vendingMachine.Coin;
import tdd.vendingMachine.Product;
import tdd.vendingMachine.Shelf;

public abstract class AbstractState implements State {

    protected final StateFactory factory;
    protected final StateContext context;

    AbstractState(StateFactory factory, StateContext context) {
        this.factory = factory;
        this.context = context;
    }

    @Override
    public void onKeyboard(int number) {
        // by default do nothing
    }

    @Override
    public void onCoinInsert(Coin coin) {
        // by default return all coins
        context.returnCoin(coin);
    }

    @Override
    public void onCommand(String cmd) {
        context.display("Unknown command: " + cmd);
    }

    protected String listShelfStatus() {
        final StringBuilder sb = new StringBuilder();
        final Shelf[] shelves = context.getShelves();
        for (int i = 0; i < shelves.length; i++) {
            sb.append(i).append(": ");
            sb.append(shelves[i].product
                    .map(p -> String.format("%s, cost: %s", p.name, Coin.formatAsDecimal(p.cost)))
                    .orElse("None"));
            sb.append(", count: ").append(shelves[i].count).append("\n");
        }
        return sb.toString();
    }

}
