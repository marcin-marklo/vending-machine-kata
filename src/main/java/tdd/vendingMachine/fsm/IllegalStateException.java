package tdd.vendingMachine.fsm;

class IllegalStateException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	IllegalStateException(String msg) {
        super(msg);
    }
}
