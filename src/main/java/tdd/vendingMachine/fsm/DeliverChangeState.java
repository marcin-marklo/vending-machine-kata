package tdd.vendingMachine.fsm;

class DeliverChangeState extends AbstractState {

    private final int change;

    DeliverChangeState(StateFactory factory, StateContext context, int change) {
        super(factory, context);
        this.change = change;
    }

    @Override
    public void onEnter() {
        if (change < 0) {
            throw new IllegalStateException("Change cannot be negative!");
        }
        if (change > 0) {
            context.display("Please retrieve your change.\n");
            context.getMoneyKeeper().getMoney(change).forEach(c -> context.returnCoin(c));
        }
        context.changeState(factory.getIdleState(context));
    }

}
