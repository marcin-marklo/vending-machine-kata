package tdd.vendingMachine.fsm;

import tdd.vendingMachine.Shelf;

public interface StateFactory {

    public State getIdleState(StateContext context);
    public State getInsertCoinsState(StateContext context, Shelf shelf);
    public State getDeliverProductState(StateContext context, Shelf shelf, int change);
    public State getDeliverChangeState(StateContext context, int change);
    public State getMainServiceState(StateContext context);
    public State getModifyShelfState(StateContext context, Shelf shelf);
    public State getModifyProductsState(StateContext context);

}
