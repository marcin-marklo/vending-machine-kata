package tdd.vendingMachine.fsm;

class MainServiceState extends AbstractServiceState {

    static final String COMMAND_MODIFY_PRODUCT = "MODPROD";

    MainServiceState(StateFactory factory, StateContext context) {
        super(factory, context);
    }

    @Override
    public void onEnter() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Welcome to the main service state!\n");
        sb.append("All inserted coins will be kept.\n");
        sb.append("Shelf status: \n");
        sb.append(listShelfStatus());
        sb.append("Enter shelf number to modify that shelf\n");
        sb.append("Other commands:\n");
        sb.append(COMMAND_CANCEL).append(" - return to the idle state\n");
        sb.append(COMMAND_MODIFY_PRODUCT).append(" - enter product modification mode.\n");
        context.display(sb.toString());
    }

    @Override
    public void onCommand(String cmd) {
        switch (cmd) {
        case State.COMMAND_CANCEL:
            context.changeState(factory.getIdleState(context));
            break;
        case COMMAND_MODIFY_PRODUCT:
            context.changeState(factory.getModifyProductsState(context));
            break;
        default:
            super.onCommand(cmd);
        }
    }

    @Override
    public void onKeyboard(int number) {
        if ((number < 0) || (number >= context.getShelves().length)) {
            context.display("Invalid shelf number!");
            return;
        }
        context.changeState(
                factory.getModifyShelfState(context, context.getShelves()[number]));
    }

}
