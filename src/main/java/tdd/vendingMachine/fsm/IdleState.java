package tdd.vendingMachine.fsm;

import tdd.vendingMachine.Shelf;

class IdleState extends AbstractState {

    static final String COMMAND_SERVICE = "SERVICE";

    IdleState(StateFactory factory, StateContext context) {
        super(factory, context);
    }

    @Override
    public void onKeyboard(int number) {
        final Shelf[] shelves = context.getShelves();
        if ((number < 0) || (number >= shelves.length)) {
            context.display("Invalid selection!");
            return;
        }
        final Shelf selected = shelves[number];
        if (selected.count == 0) {
            context.display("Selected shelf is empty.");
            return;
        }
        context.changeState(factory.getInsertCoinsState(context, selected));
    }

    @Override
    public void onEnter() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Welcome to the vending machine!\n");
        sb.append("Available shelves:\n");
        sb.append(listShelfStatus());
        sb.append("Please make your selection or type ")
        .append(COMMAND_SERVICE)
        .append(" to enter service mode.\n");
        context.display(sb.toString());
    }

    @Override
    public void onCommand(String cmd) {
        switch (cmd) {
        case COMMAND_SERVICE:
            context.changeState(factory.getMainServiceState(context));
            break;
        default:
            super.onCommand(cmd);
        }
    }

}
