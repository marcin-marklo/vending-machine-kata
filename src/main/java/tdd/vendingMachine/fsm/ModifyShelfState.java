package tdd.vendingMachine.fsm;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tdd.vendingMachine.Product;
import tdd.vendingMachine.Shelf;

class ModifyShelfState extends AbstractServiceState {

    static final String COMMAND_SET = "SET";
    static final String COMMAND_CLEAR = "CLEAR";
    private static final Pattern setCommandPattern = Pattern.compile(COMMAND_SET + " ([A-Za-z].*?) (-?\\d+)");

    private final Shelf shelf;

    ModifyShelfState(StateFactory factory, StateContext context, Shelf shelf) {
        super(factory, context);
        this.shelf = shelf;
    }

    @Override
    public void onEnter() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Current shelf: ");
        sb.append(shelf.product.map(p -> p.name).orElse("None"));
        sb.append(", count: ").append(shelf.count).append("\n");
        sb.append("Available commands:\n");
        sb.append(COMMAND_CANCEL).append(" - return to the main service state\n");
        sb.append(COMMAND_SET).append(" <Product name> <count> -set the product on the shelf\n");
        sb.append(COMMAND_CLEAR).append(" - clear the shelf");
        context.display(sb.toString());
    }

    @Override
    public void onCommand(String cmd) {
        if (cmd.equals(State.COMMAND_CANCEL)) {
            context.changeState(factory.getMainServiceState(context));
            return;
        }
        if (cmd.startsWith(COMMAND_SET) && executeSetCommand(cmd)) {
            return;
        }
        if (cmd.equals(COMMAND_CLEAR)) {
            shelf.product = null;
            shelf.count = 0;
            return;
        }
        super.onCommand(cmd);
    }

    private boolean executeSetCommand(String cmd) {
        final Matcher matcher = setCommandPattern.matcher(cmd);
        if (!matcher.matches()) {
            return false;
        }
        final String name = matcher.group(1);
        final int count = Integer.parseInt(matcher.group(2));
        final Optional<Product> maybeProduct = context.getProductManager()
                .getProductByName(name);
        if (!maybeProduct.isPresent()) {
            context.display("Product does not exist: " + name);
            return false;
        }
        if (count < 0) {
            context.display("Count must be positive");
            return false;
        }
        shelf.product = maybeProduct;
        shelf.count = count;
        return true;
    }

}
