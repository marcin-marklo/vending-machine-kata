package tdd.vendingMachine.fsm;

import tdd.vendingMachine.Coin;

interface State {

    public static final String COMMAND_CANCEL = "CANCEL";

    void onEnter();

    void onKeyboard(int number);

    void onCoinInsert(Coin coin);

    void onCommand(String cmd);

}
