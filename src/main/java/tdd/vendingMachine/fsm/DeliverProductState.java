package tdd.vendingMachine.fsm;

import tdd.vendingMachine.Product;
import tdd.vendingMachine.Shelf;

class DeliverProductState extends AbstractState {

    private final Shelf shelf;
    private final int change;

    DeliverProductState(StateFactory factory, StateContext context, Shelf shelf, int change) {
        super(factory, context);
        this.shelf = shelf;
        this.change = change;
    }

    @Override
    public void onEnter() {
        if (shelf.count <= 0) {
            throw new IllegalStateException("Shelf is empty!");
        }
        shelf.count--;
        context.deliverProduct(new Product(shelf.product.get()));
        context.changeState(factory.getDeliverChangeState(context, change));
    }

}
