package tdd.vendingMachine;

import java.util.Optional;

public interface ProductManager {

    public void addProduct(String name, int cost) throws InvalidOperationException;

    public void modifyProduct(String productName, String newName)  throws InvalidOperationException;

    public void modifyProduct(String productName, int newCost)  throws InvalidOperationException;

    public void modifyProduct(String productName, String newName, int newCost)  throws InvalidOperationException;

    public Optional<Product> getProductByName(String name);

    public void removeProduct(Product product);

    public String listProducts();

    public static class InvalidOperationException extends Exception {

        private static final long serialVersionUID = 1L;

        protected InvalidOperationException(String message) {
            super(message);
        }
    }

}
