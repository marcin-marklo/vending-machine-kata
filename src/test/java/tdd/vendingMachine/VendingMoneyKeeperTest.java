package tdd.vendingMachine;

import java.util.Collection;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class VendingMoneyKeeperTest {

    @Test
    public void addSingleCoin() {
        final VendingMoneyKeeper keeper = new VendingMoneyKeeper();
        for (Coin coin : Coin.values()) {
            Assertions.assertThat(keeper.coins.getOrDefault(coin, 0)).isEqualTo(0);
        }
        keeper.addMoney(Coin.FIVE);
        for (Coin coin : Coin.values()) {
            Assertions.assertThat(keeper.coins.getOrDefault(coin, 0)).isEqualTo(coin == Coin.FIVE ? 1 : 0);
        }
    }
    
    @Test
    public void canGetZeroOrLess() {
        final MoneyKeeper keeper = new VendingMoneyKeeper();
        Assertions.assertThat(keeper.canGetMoney(0)).isTrue();
    }
    
    @Test
    public void canGetMoney() {
        final MoneyKeeper keeper = new VendingMoneyKeeper();
        keeper.addMoney(Coin.FIVE);
        keeper.addMoney(Coin.TWO);
        keeper.addMoney(Coin.ONE);
        keeper.addMoney(Coin.ONE);
        keeper.addMoney(Coin.ONE);
        keeper.addMoney(Coin.HALF);
        Assertions.assertThat(keeper.canGetMoney(500)).isTrue();
        Assertions.assertThat(keeper.canGetMoney(400)).isTrue();
    }
    
    @Test
    public void canGetTooMuch() {
        final MoneyKeeper keeper = new VendingMoneyKeeper();
        keeper.addMoney(Coin.HALF);
        Assertions.assertThat(keeper.canGetMoney(500)).isFalse();
    }
    
    @Test
    public void getZeroOrLess() {
        final MoneyKeeper keeper = new VendingMoneyKeeper();
        final Collection<Coin> coins = keeper.getMoney(0);
        Assertions.assertThat(coins).isEmpty();
    }
    
    @Test
    public void getMoney() {
        final MoneyKeeper keeper = new VendingMoneyKeeper();
        keeper.addMoney(Coin.FIVE);
        keeper.addMoney(Coin.TWO);
        keeper.addMoney(Coin.ONE);
        keeper.addMoney(Coin.ONE);
        keeper.addMoney(Coin.ONE);
        keeper.addMoney(Coin.HALF);
        final Collection<Coin> coins = keeper.getMoney(400);
        int amount = coins.stream().mapToInt(c -> c.denomination).sum();
        Assertions.assertThat(amount).isEqualTo(400);
    }
    
    @Test
    public void getTooMuchMoney() {
        final MoneyKeeper keeper = new VendingMoneyKeeper();
        Assertions.assertThatThrownBy(() -> keeper.getMoney(500))
        .isInstanceOf(VendingMoneyKeeper.NotEnoughMoneyException.class);
    }
    
    @Test
    public void removeExistingCoin() {
        final VendingMoneyKeeper keeper = new VendingMoneyKeeper();
        keeper.addMoney(Coin.FIVE);
        keeper.addMoney(Coin.FIVE);
        Assertions.assertThat(keeper.removeCoin(Coin.FIVE)).isTrue();
        Assertions.assertThat(keeper.coins.getOrDefault(Coin.FIVE, 0)).isEqualTo(1);
    }
    
    @Test
    public void removeNonExistingCoin() {
        final VendingMoneyKeeper keeper = new VendingMoneyKeeper();
        keeper.addMoney(Coin.FIVE);
        keeper.addMoney(Coin.FIVE);
        Assertions.assertThat(keeper.removeCoin(Coin.ONE)).isFalse();
        for (Coin coin : Coin.values()) {
            Assertions.assertThat(keeper.coins.getOrDefault(coin, 0)).isEqualTo(coin == Coin.FIVE ? 2 : 0);
        }
    }

}
