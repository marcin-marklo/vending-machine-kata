package tdd.vendingMachine;

import java.util.Collection;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class VendingProductManagerTest {

    @Test
    public void addProduct() throws Exception {
        final VendingProductManager manager = new VendingProductManager();
        manager.addProduct("Tee", 10);
        final Collection<Product> products = manager.getProducts();
        Assertions.assertThat(products).containsExactly(new Product("Tee", 10));
    }

    @Test
    public void addDuplicatedProduct() throws Exception {
        final VendingProductManager manager = new VendingProductManager();
        manager.addProduct("Tee", 10);
        Assertions.assertThatThrownBy(() -> manager.addProduct("Tee", 20))
        .isInstanceOf(ProductManager.InvalidOperationException.class);
        final Collection<Product> products = manager.getProducts();
        Assertions.assertThat(products).containsExactly(new Product("Tee", 10));
    }

    @Test
    public void addProductWithInvalidCost() {
        final VendingProductManager manager = new VendingProductManager();
        Assertions.assertThatThrownBy(() -> manager.addProduct("Tee", 1))
        .isInstanceOf(ProductManager.InvalidOperationException.class);
    }

    @Test
    public void addProductWithNegativeCost() {
        final VendingProductManager manager = new VendingProductManager();
        Assertions.assertThatThrownBy(() -> manager.addProduct("Tee", -10))
        .isInstanceOf(ProductManager.InvalidOperationException.class);
    }

    @Test
    public void getProductByName() throws Exception {
        final VendingProductManager manager = new VendingProductManager();
        manager.addProduct("Tee", 10);
        final Optional<Product> someProduct = manager.getProductByName("Tee");
        Assertions.assertThat(someProduct).isPresent();
        final Product product = someProduct.get();
        Assertions.assertThat(product.name).isEqualTo("Tee");
        Assertions.assertThat(product.cost).isEqualTo(10);
    }

    @Test
    public void getNonexistentProductByName() {
        final VendingProductManager manager = new VendingProductManager();
        final Optional<Product> someProduct = manager.getProductByName("Tee");
        Assertions.assertThat(someProduct.isPresent()).isFalse();
    }

    @Test
    public void changeProductName() throws Exception {
        final VendingProductManager manager = new VendingProductManager();
        manager.addProduct("Tee", 10);
        final Product product = manager.getProductByName("Tee").get();
        manager.modifyProduct("Tee", "Black Tee");
        Assertions.assertThat(product.name).isEqualTo("Black Tee");
        Assertions.assertThat(product.cost).isEqualTo(10);
    }

    @Test
    public void changeProductToEmptyName() throws Exception {
        final VendingProductManager manager = new VendingProductManager();
        manager.addProduct("Tee", 10);
        Assertions.assertThatThrownBy(() -> manager.modifyProduct("Tee", ""))
        .isInstanceOf(ProductManager.InvalidOperationException.class);
        Assertions.assertThatThrownBy(() -> manager.modifyProduct("Tee", null))
        .isInstanceOf(ProductManager.InvalidOperationException.class);
        final Optional<Product> maybeProduct = manager.getProductByName("Tee");
        Assertions.assertThat(maybeProduct).isPresent();
        Assertions.assertThat(maybeProduct.get().name).isEqualTo("Tee");
    }

    @Test
    public void changeNonexistentProduct() {
        final VendingProductManager manager = new VendingProductManager();
        Assertions.assertThatThrownBy(() -> manager.modifyProduct("Tee", ""))
        .isInstanceOf(ProductManager.InvalidOperationException.class);
    }

    @Test
    public void changeProductCost() throws Exception {
        final VendingProductManager manager = new VendingProductManager();
        manager.addProduct("Tee", 10);
        manager.modifyProduct("Tee", 20);
        final Product product = manager.getProductByName("Tee").get();
        Assertions.assertThat(product.name).isEqualTo("Tee");
        Assertions.assertThat(product.cost).isEqualTo(20);
    }

    @Test
    public void changeProductCostToNegative() throws Exception {
        final VendingProductManager manager = new VendingProductManager();
        manager.addProduct("Tee", 10);
        Assertions.assertThatThrownBy(() -> manager.modifyProduct("Tee", -20))
        .isInstanceOf(ProductManager.InvalidOperationException.class);
        final Optional<Product> maybeProduct = manager.getProductByName("Tee");
        Assertions.assertThat(maybeProduct).isPresent();
        Assertions.assertThat(maybeProduct.get().cost).isEqualTo(10);
    }

    @Test
    public void changeProductCostToInvalid() throws Exception {
        final VendingProductManager manager = new VendingProductManager();
        manager.addProduct("Tee", 10);
        Assertions.assertThatThrownBy(() -> manager.modifyProduct("Tee", 1))
        .isInstanceOf(ProductManager.InvalidOperationException.class);
        final Optional<Product> maybeProduct = manager.getProductByName("Tee");
        Assertions.assertThat(maybeProduct).isPresent();
        Assertions.assertThat(maybeProduct.get().cost).isEqualTo(10);
    }

    @Test
    public void changeNonexistentProductCost() {
        final VendingProductManager manager = new VendingProductManager();
        Assertions.assertThatThrownBy(() -> manager.modifyProduct("Tee", 20))
        .isInstanceOf(ProductManager.InvalidOperationException.class);
    }

    @Test
    public void removeProduct() throws Exception {
        final VendingProductManager manager = new VendingProductManager();
        manager.addProduct("Tee", 10);
        manager.addProduct("Black Tee", 10);
        final Product tee = manager.getProductByName("Tee").get();
        manager.removeProduct(tee);
        Assertions.assertThat(manager.getProductByName("Tee").isPresent()).isFalse();
        Assertions.assertThat(manager.getProductByName("Black Tee")).isPresent();
    }

}
