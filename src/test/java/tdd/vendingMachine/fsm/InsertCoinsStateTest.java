package tdd.vendingMachine.fsm;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import tdd.vendingMachine.Coin;
import tdd.vendingMachine.MoneyKeeper;
import tdd.vendingMachine.Product;
import tdd.vendingMachine.Shelf;

public class InsertCoinsStateTest {

    private final StateFactory factory = new MockStateFactory() {

        @Override
        public State getInsertCoinsState(StateContext context, Shelf shelf) {
            return new InsertCoinsState(this, context, shelf);
        }
    };

    private StateContext context;
    private MoneyKeeper keeper;

    @Before
    public void setup() {
        context = Mockito.mock(StateContext.class);
        keeper = Mockito.mock(MoneyKeeper.class);
        Mockito.when(context.getMoneyKeeper()).thenReturn(keeper);
        Mockito.when(keeper.canGetMoney(Mockito.anyInt())).thenReturn(true);
    }

    @Test
    public void insertExactMoney() {
        final Shelf shelf = new Shelf(new Product("Coffee", 230), 1);
        final State state = factory.getInsertCoinsState(context, shelf);
        state.onCoinInsert(Coin.TWO);
        Mockito.verify(context, Mockito.never()).changeState(Mockito.any());
        Mockito.verify(keeper).addMoney(Coin.TWO);
        state.onCoinInsert(Coin.ONE_FIFTH);
        Mockito.verify(context, Mockito.never()).changeState(Mockito.any());
        Mockito.verify(keeper).addMoney(Coin.ONE_FIFTH);
        state.onCoinInsert(Coin.ONE_TENTH);
        Mockito.verify(keeper).addMoney(Coin.ONE_TENTH);
        final State expected = factory.getDeliverProductState(context, shelf, 0);
        Mockito.verify(context).changeState(expected);
    }

    @Test
    public void insertTooMuchMoneyAndCanGetChange() {
        final Shelf shelf = new Shelf(new Product("Coffee", 200), 1);
        final State state = factory.getInsertCoinsState(context, shelf);
        state.onCoinInsert(Coin.FIVE);
        final State expected = factory.getDeliverProductState(context, shelf, 300);
        Mockito.verify(context).changeState(expected);
    }

    @Test
    public void insertTooMuchMoneyAndCanNotGetChange() {
        final Shelf shelf = new Shelf(new Product("Coffee", 200), 1);
        Mockito.when(keeper.canGetMoney(Mockito.anyInt())).thenReturn(false);
        final State state = factory.getInsertCoinsState(context, shelf);
        state.onCoinInsert(Coin.FIVE);
        final State expected = factory.getIdleState(context);
        Mockito.verify(context).changeState(expected);
        Mockito.verify(context).returnCoin(Coin.FIVE);
    }

    @Test
    public void cancel() {
        final Shelf shelf = new Shelf(new Product("Coffee", 200), 1);
        final State state = factory.getInsertCoinsState(context, shelf);
        state.onCoinInsert(Coin.ONE);
        state.onCommand(State.COMMAND_CANCEL);
        final State expected = factory.getIdleState(context);
        Mockito.verify(context).changeState(expected);
        Mockito.verify(context).returnCoin(Coin.ONE);
    }

}
