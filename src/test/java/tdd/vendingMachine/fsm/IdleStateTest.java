package tdd.vendingMachine.fsm;

import org.junit.Test;
import org.mockito.Mockito;

import tdd.vendingMachine.Coin;
import tdd.vendingMachine.Product;
import tdd.vendingMachine.Shelf;

public class IdleStateTest {

    private final StateFactory stateFactory = new MockStateFactory() {

        @Override
        public State getIdleState(StateContext context) {
            return new IdleState(this, context);
        }

    };

    @Test
    public void selectNonexistentShelf() {
        final StateContext context = Mockito.mock(StateContext.class);
        final State idleState = stateFactory.getIdleState(context);
        Mockito.when(context.getShelves()).thenReturn(new Shelf[0]);
        idleState.onKeyboard(-1);
        Mockito.verify(context, Mockito.never()).changeState(Mockito.any());
        idleState.onKeyboard(0);
        Mockito.verify(context, Mockito.never()).changeState(Mockito.any());
    }

    @Test
    public void selectEmptyShelf() {
        final StateContext context = Mockito.mock(StateContext.class);
        final Shelf[] shelves = new Shelf[] {new Shelf(new Product("Foo", 1), 0)};
        Mockito.when(context.getShelves()).thenReturn(shelves);
        final State idleState = stateFactory.getIdleState(context);
        idleState.onKeyboard(0);
        Mockito.verify(context, Mockito.never()).changeState(Mockito.any());
    }

    @Test
    public void selectNonEmptyShelf() {
        final StateContext context = Mockito.mock(StateContext.class);
        final Shelf[] shelves = new Shelf[] {new Shelf(new Product("Foo", 1), 5)};
        Mockito.when(context.getShelves()).thenReturn(shelves);
        final State idleState = stateFactory.getIdleState(context);
        idleState.onKeyboard(0);
        final State expected = stateFactory.getInsertCoinsState(context, shelves[0]);
        Mockito.verify(context).changeState(expected);
    }

    @Test
    public void insertCoins() {
        final StateContext context = Mockito.mock(StateContext.class);
        final State idleState = stateFactory.getIdleState(context);
        idleState.onCoinInsert(Coin.ONE);
        Mockito.verify(context).returnCoin(Coin.ONE);
        Mockito.verify(context, Mockito.never()).changeState(Mockito.any());
    }

    @Test
    public void enterServiceState() {
        final StateContext context = Mockito.mock(StateContext.class);
        final State state = stateFactory.getIdleState(context);
        state.onCommand(IdleState.COMMAND_SERVICE);
        final State expected = stateFactory.getMainServiceState(context);
        Mockito.verify(context).changeState(expected);
    }

}
