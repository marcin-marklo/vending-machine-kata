package tdd.vendingMachine.fsm;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import tdd.vendingMachine.Product;
import tdd.vendingMachine.ProductManager;

public class ModifyProductsStateTest {

    private final StateFactory stateFactory = new MockStateFactory() {

        @Override
        public State getModifyProductsState(StateContext context) {
            return new ModifyProductsState(this, context);
        }

    };

    private StateContext context;
    private State state;

    @Before
    public void setup() {
        context = Mockito.mock(StateContext.class);
        state = stateFactory.getModifyProductsState(context);
    }

    @Test
    public void exitToMainServiceState() {
        state.onCommand(State.COMMAND_CANCEL);
        final State expected = stateFactory.getMainServiceState(context);
        Mockito.verify(context).changeState(expected);
    }

    @Test
    public void modifyName() throws Exception {
        ProductManager manager = Mockito.mock(ProductManager.class);
        Mockito.when(context.getProductManager()).thenReturn(manager);
        state.onCommand(ModifyProductsState.MODIFY_COMMAND + " Tea -> Black Tea");
        Mockito.verify(manager).modifyProduct("Tea", "Black Tea");
    }

    @Test
    public void modifyNameAndCost() throws Exception {
        ProductManager manager = Mockito.mock(ProductManager.class);
        Mockito.when(context.getProductManager()).thenReturn(manager);
        state.onCommand(ModifyProductsState.MODIFY_COMMAND + " Tea -> Black Tea 0.2");
        Mockito.verify(manager).modifyProduct("Tea", "Black Tea", 20);
    }

    @Test
    public void modifyCost() throws Exception {
        ProductManager manager = Mockito.mock(ProductManager.class);
        Mockito.when(context.getProductManager()).thenReturn(manager);
        state.onCommand(ModifyProductsState.MODIFY_COMMAND + " Tea -> 0.2");
        Mockito.verify(manager).modifyProduct("Tea", 20);
    }

    @Test
    public void modifyEmptyParameters() {
        ProductManager manager = Mockito.mock(ProductManager.class);
        Mockito.when(context.getProductManager()).thenReturn(manager);
        state.onCommand(ModifyProductsState.MODIFY_COMMAND + " Tea");
        Mockito.verifyZeroInteractions(manager);
    }

    @Test
    public void addProduct() throws Exception {
        ProductManager manager = Mockito.mock(ProductManager.class);
        Mockito.when(context.getProductManager()).thenReturn(manager);
        state.onCommand(ModifyProductsState.ADD_COMMAND + " Black Tea 0.1");
        Mockito.verify(manager).addProduct("Black Tea", 10);
    }

    @Test
    public void addProductWithMissingFields() throws Exception {
        ProductManager manager = Mockito.mock(ProductManager.class);
        Mockito.when(context.getProductManager()).thenReturn(manager);
        state.onCommand(ModifyProductsState.ADD_COMMAND + " Tea");
        Mockito.verifyZeroInteractions(manager);
        state.onCommand(ModifyProductsState.ADD_COMMAND + " 0.1");
        Mockito.verifyZeroInteractions(manager);
    }

    @Test
    public void removeProduct() {
        final Product product = new Product("Tea", 10);
        ProductManager manager = Mockito.mock(ProductManager.class);
        Mockito.when(manager.getProductByName("Tea")).thenReturn(Optional.of(product));
        Mockito.when(context.getProductManager()).thenReturn(manager);
        state.onCommand(ModifyProductsState.DELETE_COMMAND + " Tea 0.1");
        Mockito.verify(manager).removeProduct(product);
    }

}
