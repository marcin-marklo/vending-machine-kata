package tdd.vendingMachine.fsm;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import tdd.vendingMachine.Coin;
import tdd.vendingMachine.MoneyKeeper;
import tdd.vendingMachine.Product;
import tdd.vendingMachine.Shelf;

public class MainServiceStateTest {

    private final StateFactory stateFactory = new MockStateFactory() {

        @Override
        public State getMainServiceState(StateContext context) {
            return new MainServiceState(this, context);
        }

    };

    private StateContext context;
    private State state;

    @Before
    public void setup() {
        context = Mockito.mock(StateContext.class);
        state = stateFactory.getMainServiceState(context);
    }

    @Test
    public void returnToIdleState() {
        state.onCommand(State.COMMAND_CANCEL);
        final State expected = stateFactory.getIdleState(context);
        Mockito.verify(context).changeState(expected);
    }

    @Test
    public void keepInsertedMoney() {
        final MoneyKeeper keeper = Mockito.mock(MoneyKeeper.class);
        Mockito.when(context.getMoneyKeeper()).thenReturn(keeper);
        state.onCoinInsert(Coin.FIVE);
        state.onCoinInsert(Coin.HALF);
        Mockito.verify(context, Mockito.never()).returnCoin(Mockito.any());
        Mockito.verify(keeper).addMoney(Coin.FIVE);
        Mockito.verify(keeper).addMoney(Coin.HALF);
    }

    @Test
    public void enterModifyProductsMode() {
        state.onCommand(MainServiceState.COMMAND_MODIFY_PRODUCT);
        final State expected = stateFactory.getModifyProductsState(context);
        Mockito.verify(context).changeState(expected);
    }

    @Test
    public void modifyExistingShelf() {
        final Shelf shelf = new Shelf(new Product("Tee", 100), 2);
        final Shelf[] shelves = new Shelf[] {shelf};
        Mockito.when(context.getShelves()).thenReturn(shelves);
        state.onKeyboard(0);
        final State expected = stateFactory.getModifyShelfState(context, shelf);
        Mockito.verify(context).changeState(expected);
    }

    @Test
    public void modifyNonExistentShelf() {
        Mockito.when(context.getShelves()).thenReturn(new Shelf[0]);
        state.onKeyboard(0);
        Mockito.verify(context, Mockito.never()).changeState(Mockito.any());
    }

}
