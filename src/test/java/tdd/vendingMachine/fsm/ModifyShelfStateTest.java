package tdd.vendingMachine.fsm;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;

import tdd.vendingMachine.Product;
import tdd.vendingMachine.ProductManager;
import tdd.vendingMachine.Shelf;

public class ModifyShelfStateTest {

    private final StateFactory stateFactory = new MockStateFactory();

    @Test
    public void returnToMainServiceState() {
        final StateContext context = Mockito.mock(StateContext.class);
        final State state = new ModifyShelfState(stateFactory, context, null);
        state.onCommand(State.COMMAND_CANCEL);
        Mockito.verify(context).changeState(stateFactory.getMainServiceState(context));
    }

    @Test
    public void setProduct() {
        final StateContext context = Mockito.mock(StateContext.class);
        final ProductManager manager = Mockito.mock(ProductManager.class);
        final Product product = new Product("Tea", 20);
        Mockito.when(manager.getProductByName("Tea")).thenReturn(Optional.of(product));
        Mockito.when(context.getProductManager()).thenReturn(manager);
        final Shelf shelf = new Shelf(null, 0);
        final State state = new ModifyShelfState(stateFactory, context, shelf);
        state.onCommand(ModifyShelfState.COMMAND_SET + " Tea 5");
        Assertions.assertThat(shelf.product).isPresent();
        Assertions.assertThat(shelf.product.get()).isEqualTo(product);
        Assertions.assertThat(shelf.count).isEqualTo(5);
    }

    @Test
    public void setNonexistentProduct() {
        final StateContext context = Mockito.mock(StateContext.class);
        final ProductManager manager = Mockito.mock(ProductManager.class);
        Mockito.when(manager.getProductByName(Mockito.anyString())).thenReturn(Optional.ofNullable(null));
        Mockito.when(context.getProductManager()).thenReturn(manager);
        final Shelf shelf = new Shelf(null, 0);
        final State state = new ModifyShelfState(stateFactory, context, shelf);
        state.onCommand(ModifyShelfState.COMMAND_SET + " Tea 5");
        Assertions.assertThat(shelf.product.isPresent()).isFalse();
        Assertions.assertThat(shelf.count).isEqualTo(0);
    }

    @Test
    public void setProductWithNegativeCount() {
        final StateContext context = Mockito.mock(StateContext.class);
        final ProductManager manager = Mockito.mock(ProductManager.class);
        final Product product = new Product("Tea", 20);
        Mockito.when(manager.getProductByName("Tea")).thenReturn(Optional.of(product));
        Mockito.when(context.getProductManager()).thenReturn(manager);
        final Shelf shelf = new Shelf(null, 0);
        final State state = new ModifyShelfState(stateFactory, context, shelf);
        state.onCommand(ModifyShelfState.COMMAND_SET + " Tea -5");
        Assertions.assertThat(shelf.product.isPresent()).isFalse();;
        Assertions.assertThat(shelf.count).isEqualTo(0);
    }

    @Test
    public void clearShelf() {
        final StateContext context = Mockito.mock(StateContext.class);
        final Product product = new Product("Tea", 20);
        final Shelf shelf = new Shelf(product, 10);
        final State state = new ModifyShelfState(stateFactory, context, shelf);
        state.onCommand(ModifyShelfState.COMMAND_CLEAR);
        Assertions.assertThat(shelf.product).isNull();;
        Assertions.assertThat(shelf.count).isEqualTo(0);
    }

}
