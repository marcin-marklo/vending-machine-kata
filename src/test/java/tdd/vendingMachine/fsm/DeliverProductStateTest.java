package tdd.vendingMachine.fsm;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import tdd.vendingMachine.Product;
import tdd.vendingMachine.Shelf;
import tdd.vendingMachine.fsm.DeliverProductState;
import tdd.vendingMachine.fsm.IllegalStateException;
import tdd.vendingMachine.fsm.State;
import tdd.vendingMachine.fsm.StateContext;
import tdd.vendingMachine.fsm.StateFactory;

public class DeliverProductStateTest {

    private final StateFactory factory = new MockStateFactory() {
        @Override
        public State getDeliverProductState(StateContext context, Shelf shelf, int change) {
            return new DeliverProductState(this, context, shelf, change);
        }

    };

    @Test
    public void deliverProduct() {
        final Shelf shelf = new Shelf(new Product("Coffee", 200), 2);
        final StateContext context = Mockito.mock(StateContext.class);
        final State state = factory.getDeliverProductState(context, shelf, 0);
        state.onEnter();
        final State expected = factory.getDeliverChangeState(context, 0);
        Mockito.verify(context).changeState(expected);
        final ArgumentCaptor<Product> productCaptor = ArgumentCaptor.forClass(Product.class);
        Mockito.verify(context).deliverProduct(productCaptor.capture());
        Assertions.assertThat(productCaptor.getValue().name).isEqualTo(shelf.product.get().name);
        Assertions.assertThat(shelf.count).isEqualTo(1);
    }

    @Test
    public void failOnNoProductToDeliver() {
        final Shelf shelf = new Shelf(new Product("Coffee", 200), 0);
        final StateContext context = Mockito.mock(StateContext.class);
        final State state = factory.getDeliverProductState(context, shelf, 0);
        Assertions.assertThatThrownBy(() -> state.onEnter()).isInstanceOf(IllegalStateException.class);
        Assertions.assertThat(shelf.count).isEqualTo(0);
    }
}
