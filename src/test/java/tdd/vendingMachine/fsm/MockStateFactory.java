package tdd.vendingMachine.fsm;

import tdd.vendingMachine.Coin;
import tdd.vendingMachine.Shelf;
import tdd.vendingMachine.fsm.State;
import tdd.vendingMachine.fsm.StateContext;
import tdd.vendingMachine.fsm.StateFactory;

class MockStateFactory implements StateFactory {

    @Override
    public State getIdleState(StateContext context) {
        return new MockState("Idle", new Object[0]);
    }

    @Override
    public State getInsertCoinsState(StateContext context, Shelf shelf) {
        return new MockState("InsertCoins", new Object[] {shelf});
    }

    @Override
    public State getDeliverProductState(StateContext context, Shelf shelf, int change) {
        return new MockState("DeliverProduct", new Object[] {shelf, change});
    }

    @Override
    public State getDeliverChangeState(StateContext context, int change) {
        return new MockState("DeliverChange", new Object[] {change});
    }

    @Override
    public State getMainServiceState(StateContext context) {
        return new MockState("MainService", new Object[] {});
    }

    @Override
    public State getModifyShelfState(StateContext context, Shelf shelf) {
        return new MockState("ModifyShelf", new Object[] {shelf});
    }

    @Override
    public State getModifyProductsState(StateContext context) {
        return new MockState("ModifyProducts", new Object[] {});
    }

    class MockState implements State {

        final String name;
        final Object[] params;

        MockState(String name, Object[] params) {
            this.name = name;
            this.params = params;
        }

        @Override
        public void onEnter() { }

        @Override
        public void onKeyboard(int number) { }

        @Override
        public void onCoinInsert(Coin coin) { }

        @Override
        public void onCommand(String cmd) { }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof MockState)) {
                return false;
            }
            MockState other = (MockState)obj;
            if (!name.equals(other.name)) {
                return false;
            }
            if (params.length != other.params.length) {
                return false;
            }
            for (int i = 0; i < params.length; i++) {
                if (!params[i].equals(other.params[i])) {
                    return false;
                }
            }
            return true;
        }

    }

}
