package tdd.vendingMachine.fsm;

import java.util.Arrays;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;

import tdd.vendingMachine.Coin;
import tdd.vendingMachine.MoneyKeeper;
import tdd.vendingMachine.fsm.DeliverChangeState;
import tdd.vendingMachine.fsm.IllegalStateException;
import tdd.vendingMachine.fsm.State;
import tdd.vendingMachine.fsm.StateContext;
import tdd.vendingMachine.fsm.StateFactory;

public class DeliverChangeStateTest {

    private final StateFactory factory = new MockStateFactory() {;

    @Override
    public State getDeliverChangeState(StateContext context, int change) {
        return new DeliverChangeState(this, context, change);
    }
    };

    @Test
    public void deliverChange() {
        final StateContext context = Mockito.mock(StateContext.class);
        final MoneyKeeper keeper = Mockito.mock(MoneyKeeper.class);
        Mockito.when(keeper.getMoney(10)).thenReturn(Arrays.asList(Coin.ONE_TENTH));
        Mockito.when(context.getMoneyKeeper()).thenReturn(keeper);
        final State state = factory.getDeliverChangeState(context, 10);
        state.onEnter();
        final State expected = factory.getIdleState(context);
        Mockito.verify(context).changeState(expected);
        Mockito.verify(keeper).getMoney(10);
    }

    @Test
    public void raiseInvalidState() {
        final StateContext context = Mockito.mock(StateContext.class);
        final State state = factory.getDeliverChangeState(context, -10);
        Assertions.assertThatThrownBy(() -> state.onEnter())
        .isInstanceOf(IllegalStateException.class);
    }

}
