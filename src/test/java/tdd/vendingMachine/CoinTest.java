package tdd.vendingMachine;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class CoinTest {

    @Test
    public void isDescendingOrder() {
        final Coin[] coins = Coin.values();
        for (int i = 1; i < coins.length; i++) {
            Assertions.assertThat(coins[i].denomination).isLessThan(coins[i - 1].denomination);
        }
    }

}
