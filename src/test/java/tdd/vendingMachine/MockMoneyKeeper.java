package tdd.vendingMachine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MockMoneyKeeper implements MoneyKeeper {

    private final boolean alwaysHasChange;

    public MockMoneyKeeper(boolean alwaysHasChange) {
        this.alwaysHasChange = alwaysHasChange;
    }

    @Override
    public void addMoney(Coin coin) { }

    @Override
    public boolean canGetMoney(int amount) {
        return alwaysHasChange;
    }

    @Override
    public Collection<Coin> getMoney(int amount) {
        List<Coin> coins = new ArrayList<>();
        for (int i = 0; i < amount / 10; i++) {
            coins.add(Coin.ONE_TENTH);
        }
        return coins;
    }

    @Override
    public boolean removeCoin(Coin coin) {
        return true;
    }

}
